from flask import Flask, render_template, redirect, url_for, session
from get_form_info import RoomNameForm
from flask_socketio import SocketIO, join_room, leave_room,emit,send
from muhfuncs import escapeHTML
app = Flask(__name__)
app.secret_key = '5/dfug42534hds63__!)@$'
socketio = SocketIO(app)

@app.route('/', methods=['GET', 'POST'])
def index():
    form = RoomNameForm()
    if form.validate_on_submit():
        session['username']= escapeHTML(form.name.data)
        session['room_id']= escapeHTML(form.room_id.data)
        return redirect(url_for('room',room_id=session['room_id']))
    return render_template('index.html',form=form)
@app.route('/room/<room_id>', methods=['GET', 'POST'])
def room(room_id):
    return render_template('chat.html')

@socketio.on('join')
def on_join():
    user=session['username']
    room_id=session['room_id']
    print(room_id)
    join_room(room_id)
    send(user + ' has entered the room.', room=room_id)

@socketio.on('leave')
def on_leave():
    user = session['username']
    room_id=session['room_id']
    leave_room(room_id)
    send(user + ' has left the room.', room=room_id)
@socketio.on('message')
def handle_message(msg):
    msg = escapeHTML(msg)
    send("<span id='name'>" + session['username'] +":</span>"+msg,room=session['room_id'])
if __name__ == '__main__':
    socketio.run(app)
