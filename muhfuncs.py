def escapeHTML(string):
    schars=['"','<','>','\'','&','/']
    schrep=['&quot;','&lt;','&gt;','&#x27;','&amp;','&#x2F;']
    
    strchar = [c for c in string]

    for c in strchar:
        if c in schars:
            strchar[strchar.index(c)] = schrep[schars.index(c)]
    string = ''.join(strchar)
    return string

