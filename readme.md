install the python-pip and python-virtualenv packages

cd /path/to/lind
. venv/bin/activate 

For the development of lind install the following pip packages:
1. flask
2. flask_wtf
3. flask_socketio
4. eventlet

export FLASK_ENV=development
export FLASK_APP=lind.py

flask run

