from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired

class RoomNameForm(FlaskForm):
    room_id = StringField('room_id',validators=[DataRequired()])
    name = StringField('name',validators=[])

